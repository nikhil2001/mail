<?php session_start(); $email = $_SESSION['email']; ?>
<html>
	<head><script src="function.js"></script>

</head>
	<body>
		<div class="header">
      <div class="header__left" onclick="reloadMain()">
        	<h2>MAIL</h2>
      </div>
     <!--Search box-->
      <div class="header__middle" style="position: relative;left: 55px;">
        <input type="text" placeholder="Search mail" />
      </div>

    <!-- Account details and dropdown -->
    <form action="logout.php" method="POST">
      <div class="dropdown">
        <div class="dropdown-content" >
          <a style="color: grey;"><?php echo $email ?></a></br>
          <a><button  name='logoutBtn' style="cursor: pointer;background-color: transparent; border:none">Logout</button></a>
        </div>
      </div>
    </form>
    </div>

    <!-- Main Body Starts -->
  <div class="main__body">
    <div class="sidebar">
      <button class="sidebar__compose" onclick="compose()"><span class="material-icons"> add </span>Compose</button>
      <div class="sidebarOption sidebarOption__active">
        <h3 onclick="inbox()">Inbox</h3>
      </div>
      
      <div class="sidebarOption">
        <h3 onclick="sent()">Sent</h3>
      </div>
  </div>

</html>