<?php
	session_start();
	include('../includes/header.php');
	include('../includes/navbar.php');
?>

<div class="py-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-5">
		
				<?php include('../includes/message.php');?>

				<div class="card">
					<div class="card-header"><h2>Register</h2></div>
					<div class="card-body">
						<form action="FormRegisterDB.php" method ="POST">
							<div class="form-group mb-3">
								<label>First Name</label></br>
								<input required type="text" name="fname" placeholder="Enter your First Name">
							</div>
							<div class="form-group mb-3">
								<label>Last Name</label></br>
								<input required type="text" name="lname" placeholder="Enter your Last Name">
							</div>
							<div class="form-group mb-3">
								<label>Email ID</label></br>
								<input required type="email" name="email" placeholder="Enter your mail address">
							</div>
							<div class="form-group mb-3">
								<label>Key for password</label></br>
								<input required type="text" name="key" placeholder="Enter a key">
							</div>
							<div class="form-group mb-3">
								<label>Password</label></br>
								<input required type="password" name="password" placeholder="Enter Password">
							</div>
							<div class="form-group mb-3">
								<label>Confirm Password</label></br>
								<input required type="password" name="cpassword" placeholder="Enter Password Again">
							</div>
							<div class="form-group mb-3">
								<button type = " submit" name="register_bt" class="btn btn-primary">Register</button>
							</div>
						</form>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
<?php 
	include('../includes/footer.php');
?>




