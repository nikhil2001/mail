<?php
include('formconn.php');
session_start();

	if(isset($_POST['register_bt'])){
		$fname = mysqli_real_escape_string($conn,$_POST['fname']);
		$lname = mysqli_real_escape_string($conn,$_POST['lname']);
		$email = mysqli_real_escape_string($conn,$_POST['email']);
		$key = mysqli_real_escape_string($conn,$_POST['key']);
		$password = $_POST['password'];
		$cpassword = $_POST['cpassword'];

		//cheking whether password is minimus 8 characters 
		if(strlen($password)<8){
			$_SESSION['message']="Password must be atleast 8 characters";
			echo'<meta http-equiv="refresh" content="0;url=FormRegister.php">';
			exit(0);
		}

		// checking how strong the password is
		$password_pattern = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^A-Za-z\d])/"; // password constraints
		if(!preg_match($password_pattern,$password)){
			$_SESSION['message']="Password must have atleast one upper case, one lowercase, one special character and one digit ";
			echo'<meta http-equiv="refresh" content="0;url=FormRegister.php">';
			exit(0);
		}
		      
		if($password == $cpassword){
			//check for mail (already present in db)
			$checkmail = "SELECT email FROM users WHERE email='$email'";
			$checkmail_run = mysqli_query($conn,$checkmail);
			if(mysqli_num_rows($checkmail_run) > 0 ){
				//email exist
				$_SESSION['message']="Email Already exists";
				echo'<meta http-equiv="refresh" content="0;url=FormRegister.php">';
				exit(0);
			}else{
				
				//creating user
				$user_query = "INSERT INTO users (fname,lname,email,passkey,pass)VALUES('$fname','$lname','$email','$key','$password')";
				$user_query_run = $conn->query($user_query);

				//need to redirect to login page after successfull registration 
				if($user_query_run){
					$_SESSION['message']="Sucessfully registered, Login to continue";
					echo'<meta http-equiv="refresh" content="0;url=FormLogin.php">';
					exit(0);
				}
				else{
					$_SESSION['message']="Something went wrong ,try again";
					echo'<meta http-equiv="refresh" content="0;url=FormRegister.php">';
					exit(0);
				}
			}
		}else{
			$_SESSION['message']="paswords do not match";
			echo'<meta http-equiv="refresh" content="0;url=FormRegister.php">';
			exit(0);
		}
		
	}else{
		$_SESSION['message']="connection failed";
		echo'<meta http-equiv="refresh" content="0;url=FormRegister.php">';
		exit(0);
	}
?>