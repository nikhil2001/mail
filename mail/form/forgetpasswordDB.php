<?php
include('formconn.php');
session_start();

	if(isset($_POST['change_btn'])){
		$email = mysqli_real_escape_string($conn,$_POST['email']);
		$key = mysqli_real_escape_string($conn,$_POST['key']);
		$password = $_POST['password'];
		$cpassword = $_POST['cpassword'];

		$checkmail = "SELECT email FROM users WHERE email='$email'";
		$checkmail_run = mysqli_query($conn,$checkmail);
		if(mysqli_num_rows($checkmail_run)>0){
			$checkkey = "SELECT passkey FROM users WHERE email='$email' and passkey='$key'";
			$checkkey_run = mysqli_query($conn,$checkkey);
			if(mysqli_num_rows($checkkey_run)>0){
				//checking that previous and new password are not same 
				$password_query = "SELECT pass FROM users WHERE email='$email' AND pass!='$password'";
				$password_query_run= mysqli_query($conn,$password_query);
				if(mysqli_num_rows($password_query_run)>0){
					//cheking whether password is minimum 8 characters 
					if(strlen($password)<8){
						$_SESSION['message']="Password must be atleast 8 characters";
						echo'<meta http-equiv="refresh" content="0;url=FormRegister.php">';
						exit(0);
					}
	
					// checking how strong the password is
					$password_pattern = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^A-Za-z\d])/"; // password constraints
					if(!preg_match($password_pattern,$password)){
						$_SESSION['message']="Password must have atleast one upper case, one lowercase, one special character and one digit ";
						echo'<meta http-equiv="refresh" content="0;url=FormRegister.php">';
						exit(0);
					}
					if($password == $cpassword){
						$user_query = "UPDATE users SET pass = '$password' WHERE email = '$email'";
						$user_query_run = mysqli_query($conn,$user_query);
						// need to redirect to login page after successfull password change
						if($user_query_run){
							$_SESSION['message']="Sucessfully changed, Login to continue";
							echo'<meta http-equiv="refresh" content="0;url=FormLogin.php">';
							exit(0);
						}
						else{
							$_SESSION['message']="Something went wrong ,try again";
							echo'<meta http-equiv="refresh" content="0;url=FormRegister.php">';
							exit(0);
						}
					}
				}else{
					$_SESSION['message']="Your password should not be same as the previous one";
					echo'<meta http-equiv="refresh" content="0;url=forgetpassword.php">';
					exit(0);
				}
				
			}else{
				$_SESSION['message']="key for changing the password is not correct";
				echo'<meta http-equiv="refresh" content="0;url=forgetpassword.php">';
				exit(0);
			}
		}

		
		      
	}
?>