<!-- used for the purpose that inbox and sent files of any used users should not be accessed directly without login -->
<!-- this makes user-email  unset so that it couldnot be accessed-->
<?php
	session_start();
	if(isset($_POST['logoutBtn'])){
		unset($_SESSION['email']);
		header('Location:form/FormLogin.php');
	}
?>