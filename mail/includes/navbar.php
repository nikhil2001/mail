<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
      <a class="navbar-brand" onclick="window.open('FormIndex.php','_self')" style="cursor:pointer" >Mail Login</a>
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link"  onclick="window.open('FormLogin.php','_self')"style="cursor: pointer;">Login</a>
        </li>
      	<li class="nav-item">
          <a class="nav-link" onclick="window.open('FormRegister.php','_self')" style="cursor: pointer;">Register</a>
        </li> 
      </ul>
      
    </div>
  </div>
</nav>